<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Repository Design Pattern

Repository pattern separates the data access logic and maps it to the business entities in the business logic. Communication between the data access logic and the business logic  is done through interfaces.

- [Show More](https://cubettech.com/resources/blog/introduction-to-repository-design-pattern/).

## Repository Design Pattern Files
- **[Provider] : Craete Provider with commend line: php artisan make:provider repositoryDesignPatterns**
- **[Repositories] Create Repositories Folder and Repositories Files**
- **[InterFaces] Create InterFaces Folder and InterFaces Files**
- **[Controller] Create Controller Files**

## App Providers

Put provider file "repositoryDesignPatterns" To confidg/app/["providers"]