<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class repositoryDesignPatterns extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /** Bind The files paths and add path of this provider at config/app [providers] */
        $this->app->bind(
            'App\Http\Interfaces\testRepositoryInterface',
            'App\Http\Repositories\testRepository'
        );

        /** U Can Add More Than One bind Here */
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
