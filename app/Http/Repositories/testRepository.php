<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\testRepositoryInterface;

use App\User;

class testRepository implements testRepositoryInterface{


    /** Group of model as vars */
    protected $user_model;

    /** Construct to handel models */
    public function __construct(User $user){
        // This Model Just For Test
        $this->user_model = $user;
    }

    /** Use The Function That created at interface */
    public function test(){
        // Write Your Funvtion Body...
        $num1 = 10;
        $num2 = 20;
        if($num1 >= $num2){
            $total = $num1 * $num2;
        }else{
            $total = $num1 - $num2;
        }

        // Return...
        return $this->apiResponse($total , null , 200);
    }


}

?>