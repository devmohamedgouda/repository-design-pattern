<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Interfaces\testRepositoryInterface;

class testController extends Controller
{
    /** Group of model as vars */
    public $testRepositoryInterface;

    /** Construct to handel inject models */
    public function __construct(testRepositoryInterface $testRepositoryInterface){
        $this->testRepositoryInterface = $testRepositoryInterface;
    }

    /** Create Function and Call The interface Function */
    public function test(){
        return $this->testRepositoryInterface->test();
    }
}
